/*
 toggle_led.c 
 Toggles an LED on Pin 1 of PORTB on a PIC16F627. Written
 as a sample for the article on using SDCC and GPSIM in
 Linux. http://www.micahcarrick.com/v2/content/view/14/4/
 
 Compile: sdcc --debug -mpic14 -p16f628a test.c
 Simulate: gpsim -c env.conf -s test.cod

*/
 
/* Define processor and include header file. */
#define __16f628a
#include "pic/pic16f628a.h"
 
/* Setup chip configuration */
typedef unsigned int config;
config at 0x2007 __CONFIG = _CP_OFF &
 _WDT_OFF &
 _BODEN_OFF &
 _PWRTE_OFF &
 _ER_OSC_CLKOUT &
 _MCLRE_ON &
 _LVP_OFF;
 
#define b0 0x01 /* pin 1 on PORTB */
#define B_OUTPUTS 0xfa /* value used to setup TRISB */
 

/* TXSTA */
static void setup() {
    /* PORTB.1 is an output pin */ 
    TRISB = B_OUTPUTS; 
    /* USART setup */
    //BRGH = 1;                           // baudrate class
    SPBRG = 25;

    SYNC = 0; /* Asynchronous */
    SPEN = 1; /* Serial port enable*/
    TXEN = 1; /* Tx enable */
}

static void send_byte(char alpha) {
    TXREG = alpha;
}

void main(void) {
    char alpha = 'a';
    int i;

    setup();

    TXREG = alpha;
    while(1) { /* Loop forever */
        /* toggle bit 1 */
        //PORTB = (PORTB ^ b0); 
        while (TRMT);
        for (i = 0; i < 2000; i++);
        send_byte(alpha);
        alpha++;
        if (alpha > 'z')
            alpha = 'a';
    }
}
